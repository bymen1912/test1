Команды для ps1:

.\test.ps1 encode -filePath "C:\Users\Mark\Favorites\1\temp_folder\1.csv"

.\test.ps1 encrypt -inputFile "C:\Users\Mark\Favorites\1\temp_folder\temp_folder\1.csv.txt" -outputFile "C:\Users\Mark\Favorites\1\temp_folder\temp_folder\encrypted_file2.bin" -password "112233" -action "encrypt"

.\test.ps1 decrypt -inputFile "C:\Users\Mark\Favorites\1\temp_folder\temp_folder\encrypted_file_TEST.bin" -outputFile "C:\Users\Mark\Favorites\1\temp_folder\temp_folder\decrypt_file_EZ.bin" -password "112233" -action "decrypt"

.\test.ps1 decode -filePath "C:\Users\Mark\Favorites\1\temp_folder\temp_folder\decrypt_file_EZ.bin"




SCRIPT:


param (
    [string]$command,
    [string]$filePath,
    [string]$inputFile,
    [string]$outputFile,
    [string]$password,
    [string]$action  # "encrypt" or "decrypt"
)

$TempFolder = "C:\Users\Mark\Favorites\1\temp_folder\temp_folder"

if ($command -eq "encode") {
    if (-not $filePath) {
        Write-Host "Not found path file."
        exit 1
    }

    if (-not (Test-Path -Path $filePath -PathType Leaf)) {
        Write-Host "File '$filePath' Not found."
        exit 1
    }

    $encodedFilePath = Join-Path -Path $TempFolder -ChildPath "$($filePath | Split-Path -Leaf).txt"
    certutil -encode $filePath $encodedFilePath
    Write-Host "File finish encode in '$encodedFilePath'."

    Remove-Item $filePath
    Write-Host "Source file successfully deleted."

} elseif ($command -eq "decode") {
    if (-not $filePath) {
        Write-Host "No path to decode file specified."
        exit 1
    }

    if (-not (Test-Path -Path $filePath -PathType Leaf)) {
        Write-Host "File '$filePath' not found."
        exit 1
    }

    $decodedFilePath = Join-Path -Path $TempFolder -ChildPath "$($filePath | Split-Path -Leaf).csv"
    certutil -decode $filePath $decodedFilePath
    Write-Host "File successfully decoded to '$decodedFilePath'."

    Remove-Item $filePath
    Write-Host "Source file successfully deleted."

} elseif ($command -eq "encrypt" -or $command -eq "decrypt") {
    $securePassword = ConvertTo-SecureString -String $password -Force -AsPlainText
    $AESKey = (New-Object Security.Cryptography.PasswordDeriveBytes($securePassword, @(), "SHA256", 100)).GetBytes(32)
    $AESIV = (New-Object Security.Cryptography.PasswordDeriveBytes($securePassword, @(), "SHA256", 100)).GetBytes(16)

    $AES = New-Object Security.Cryptography.RijndaelManaged
    $AES.Key = $AESKey
    $AES.IV = $AESIV

    $bufferSize = 1024
    $fsInput = [System.IO.File]::OpenRead($inputFile)
    $fsOutput = [System.IO.File]::Create($outputFile)

    if ($action -eq "encrypt") {
        $encryptor = $AES.CreateEncryptor()
    } elseif ($action -eq "decrypt") {
        $decryptor = $AES.CreateDecryptor()
    } else {
        Write-Host "Invalid action specified. Use 'encrypt' or 'decrypt'."
        exit 1
    }

    if ($action -eq "encrypt") {
        $cs = [System.Security.Cryptography.CryptoStream]::new($fsOutput, $encryptor, [System.Security.Cryptography.CryptoStreamMode]::Write)
    } else {
        $cs = [System.Security.Cryptography.CryptoStream]::new($fsOutput, $decryptor, [System.Security.Cryptography.CryptoStreamMode]::Write)
    }

    $buffer = New-Object byte[] $bufferSize
    $read = $fsInput.Read($buffer, 0, $bufferSize)
    while ($read -gt 0) {
        $cs.Write($buffer, 0, $read)
        $read = $fsInput.Read($buffer, 0, $bufferSize)
    }

    $cs.Close()
    $fsInput.Close()
    $fsOutput.Close()

    if ($action -eq "encrypt") {
        Remove-Item $inputFile
    }
    
} else {
    Write-Host "Unknown command. Use 'encode', 'decode', 'encrypt', or 'decrypt'."
    exit 1
}

